## Application Overview

This application is a bot designed to provide enhanced interactivity and functionality through the integration of 
advanced AI capabilities, specifically using Language Learning Models (LLM). This bot can be seamlessly integrated 
into various systems, including Telegram, WhatsApp, Slack, Site Agent Chat, and more. It offers a range of commands that 
leverage AI to interpret and respond to user input intelligently, making it an ideal tool for customer support, 
AI agents, engagement, and educational purposes.

### Features
- **AI-Enhanced Interactions**: Incorporates a state-of-the-art LLM to understand and generate text-based responses based on user interactions.
- **Text Inference Support**: Utilizes three inference engines for diverse and robust text generation:
    - *Ollama*: For self-hosted models (Llama3, Phi3, WizardLM2, Mistral, Gemma, Falcon, etc.).
    - *OpenAI*: Integrating ChatGPT.
    - *Anthropic*: Incorporating Claude.
- **Integration Capabilities**:
    - *Telegram*
        - **Customizable Command Set**: Includes a variety of commands that can be used to control the bot's behavior and manage chat histories.
        - **Flexible Chat Management**: Supports enabling and disabling chat history, as well as targeted response capabilities, allowing it to respond to all messages or only when specifically tagged.

## Prerequisites
Before you begin the installation and setup of the Telegram bot, ensure you have the following requirements met:
- **Docker**: Install *Docker Engine* from [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/).
- **Docker Compose**: Install *Docker Compose* from [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

## Prepare the Environment

Create a .env file by copying [.env.sample](.env.sample) and fill it with actual values.

## Run the Application
```bash
docker-compose up --build
```

## Application Configuration Parameters

Below is a detailed table of all the environment variables used by the application, outlining their purpose, requirements, default values, and example values:

| Parameter                | Description                                                                                                    | Required | Default Value       | Example Value                               |
|--------------------------|----------------------------------------------------------------------------------------------------------------|----------|---------------------|---------------------------------------------|
| `BOT_NAME`               | The username of the bot.                                                                                       | Yes      | N/A                 | `my_telegram_bot`                           |
| `BOT_TOKEN`              | The token for accessing the Telegram Bot API.                                                                  | Yes      | N/A                 | `123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11` |
| `ALLOWED_CHAT_IDS`       | Comma-separated list of chat IDs allowed to interact with the bot; use `*` for all, empty list means none allowed. | No       | `[]` (no allowed chats) | `-123,456`, `*`                           |
| `MODEL_NAME`             | The model name of the LLM (Language Learning Model).                                                           | Yes      | N/A                 | `gpt-4-turbo`                               |
| `LLM_TOKEN`              | API key for accessing the LLM service.                                                                         | Yes      | N/A                 | `dfsdjfjkdf`                                |
| `LLM_ENGINE`             | The backend engine for the LLM service (e.g., `openai`).                                                       | Yes      | N/A                 | `openai`                                    |
| `LLM_URL`                | URL of the LLM service; required only for the ollama engine.                                                   | No       | N/A                 | `http://localhost:3000/ollama/api/chat`     |
| `SYSTEM_MESSAGE`         | Default system message to display when the bot interacts.                                                      | No       | "" (empty string)   | `"You are a helpful assistant."`            |
| `CLEAR_HISTORY_MESSAGE`  | Message displayed for clearing chat history.                                                                   | No       | "✅"                | "History cleared! ✅"                        |
| `ENABLE_HISTORY_MESSAGE` | Message displayed for enabling chat history.                                                                   | No       | "✅"                | "History enabled! ✅"                        |
| `DISABLE_HISTORY_MESSAGE`| Message displayed for disabling chat history.                                                                  | No       | "✅"                | "History disabled! ✅"                       |
| `RESPOND_TO_ALL_MESSAGE` | Message displayed when the bot is configured to respond to all messages.                                        | No       | "✅"                | "Responding to everyone! ✅"                 |
| `RESPOND_ON_TAG_MESSAGE` | Message displayed when the bot responds to being tagged in a message.                                          | No       | "✅"                | "Tagged response activated! ✅"              |
| `RESPOND_TO_ALL`         | Specifies whether the bot responds to all messages or not.                                                     | No       | `false`             | `true`                                      |
| `ENABLE_HISTORY`         | Specifies whether chat history is enabled for the bot.                                                         | No       | `false`             | `true`                                      |
| `MESSAGE_POLL_INTERVAL`  | The interval, in seconds, for the bot to poll for messages.                                                    | No       | `0.0`               | `2.0`                                       |
| `CONCURRENT_UPDATES`     | Specifies whether to handle incoming updates concurrently.                                                     | No       | `false`             | `true`                                      |

### Notes
- Parameters marked as **Required** must be provided for the application to function correctly.
- Optional parameters come with default values that define the bot's behavior if not explicitly set.

## Bot Commands Overview

The application supports a set of Telegram bot commands, each associated with a specific functionality designed to enhance user interaction and bot management. Below is a list of the commands and their descriptions:

| Command            | Description                                                             |
|--------------------|-------------------------------------------------------------------------|
| `/clear_history`   | Clears the chat history. This command removes all previous messages from the chat's stored history, effectively resetting the conversation context. |
| `/enable_history`  | Enables the recording of chat history. When enabled, all messages exchanged in the chat will be saved, allowing for future reference or analysis. |
| `/disable_history` | Disables the recording of chat history. This stops the bot from storing messages, helping to preserve privacy and reduce data storage needs. |
| `/respond_to_all`  | Activates the bot's response to all incoming messages. This mode is useful when the bot is intended to interact with every message it receives, regardless of specific tags or commands. |
| `/respond_on_tag`  | Enables the bot to respond only when tagged in a message. This setting focuses the bot's interactions, ensuring it replies only when explicitly mentioned within a conversation. |

### Usage
- To execute a command, type it directly into the chat interface followed by any necessary parameters (if applicable). For example, type `/enable_history` to start logging chat messages.

## Development

### Prepare the Environment

To prepare your Python environment, follow these steps. This will ensure that all dependencies are installed correctly, and your project environment is isolated.

1. **Create a Virtual Environment**  
    This isolates your Python/package environment from your global Python installation.
    ```bash
    python -m venv .venv
    ```
   
2. **Activate the Virtual Environment**
    ```bash
    source .venv/bin/activate
    ```

3. **Navigate to the Source Directory**
    ```bash
    cd src
    ```

4.  **Install Required Packages**
    ```bash
    pip install -r requirements.txt
    ```
    
### Run the Application
```bash
python main.py
```