from abc import ABC, abstractmethod
from typing import TypeVar, Generic

Bot = TypeVar("Bot")


class BotDecorator(Generic[Bot], ABC):
    """
    Abstract base class for bot decorators, providing a framework to extend
    the functionality of bot objects through decoration.
    """

    def __init__(self, bot: Bot) -> None:
        self._bot = bot

    @abstractmethod
    def run(self) -> None:
        pass
