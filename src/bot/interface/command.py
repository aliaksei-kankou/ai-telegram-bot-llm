from abc import ABC, abstractmethod
from typing import TypeVar, Generic

from telegram import Update
from telegram.ext import ContextTypes

from llm.service import LlmService
from ..type import CommandConfig

State = TypeVar("State")


class BotCommand(Generic[State], ABC):
    """Abstract base class for bot commands."""

    def __init__(self, config: CommandConfig, llm_service: LlmService) -> None:
        self._config = config
        self._llm_service = llm_service

    @abstractmethod
    async def execute(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        pass
