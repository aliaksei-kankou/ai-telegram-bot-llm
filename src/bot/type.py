from dataclasses import dataclass
from typing import Union, Optional


@dataclass
class CommandConfig:
    allowed_chat_ids: Union[str, list[int]]
    bot_name: str
    system_message: str
    clear_history_message: Optional[str] = None
    enable_history_message: Optional[str] = None
    disable_history_message: Optional[str] = None
    respond_to_all_message: Optional[str] = None
    respond_on_tag_message: Optional[str] = None
