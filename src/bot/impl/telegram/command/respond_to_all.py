from telegram import Update
from telegram.ext import ContextTypes

from llm.type import Messages, Role, Message
from .history import TelegramHistoryCommand
from .type import ChatState


class RespondToAllTelegramBotCommand(TelegramHistoryCommand):
    """Activates and configures the bot to respond to all incoming messages in a chat."""

    async def _process_message(
        self, messages: Messages, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> str:
        chat_id = update.effective_chat.id
        if chat := self._state.chats.get(chat_id):
            chat.respond_to_all = True
        else:
            system_message = Message(
                role=Role.SYSTEM, content=self._config.system_message
            )
            new_chat_state = ChatState(
                messages=[system_message],
                history_enabled=self._state.default_history_enabled,
                respond_to_all=True,
            )
            self._state.chats[chat_id] = new_chat_state

        return self._config.respond_to_all_message
