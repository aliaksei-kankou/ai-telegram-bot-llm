from telegram import Update
from telegram.ext import ContextTypes

from llm.type import Messages, Message, Role
from .history import TelegramHistoryCommand
from .type import ChatState


class DisableHistoryTelegramBotCommand(TelegramHistoryCommand):
    """Handle the disabling of chat history in a Telegram bot."""

    async def _process_message(
        self, messages: Messages, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> str:
        chat_id = update.effective_chat.id
        if chat := self._state.chats.get(chat_id):
            chat.history_enabled = False
        else:
            system_message = Message(
                role=Role.SYSTEM, content=self._config.system_message
            )
            self._state.chats[chat_id] = ChatState(
                messages=[system_message],
                history_enabled=False,
                respond_to_all=self._state.default_respond_to_all,
            )

        return self._config.disable_history_message
