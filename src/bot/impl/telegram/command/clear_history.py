from telegram import Update
from telegram.ext import ContextTypes

from llm.type import Messages, Role, Message
from .history import TelegramHistoryCommand


class ClearHistoryTelegramBotCommand(TelegramHistoryCommand):
    """Handle the clearing of chat history in a Telegram bot."""

    async def _process_message(
        self, messages: Messages, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> str:
        if chat := self._state.chats.get(update.effective_chat.id):
            system_message = Message(
                role=Role.SYSTEM, content=self._config.system_message
            )
            chat.messages = [system_message]

        return self._config.clear_history_message
