import logging
from abc import abstractmethod

from telegram import Update
from telegram.ext import ContextTypes

from llm.service import LlmService
from llm.type import Messages, Role, Message
from constant import ALL
from ....interface.command import BotCommand
from .type import ChatsState, ChatState
from ....type import CommandConfig


class TelegramHistoryCommand(BotCommand[ChatsState]):
    """Base class for Telegram commands that manipulate chat history."""

    def __init__(
        self, config: CommandConfig, llm_service: LlmService, state: ChatsState
    ):
        super().__init__(config, llm_service)
        self._state = state

    async def execute(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        chat_id = update.effective_chat.id
        text = update.message.text
        authorized = await self._is_authorized(update)

        logging.log(
            logging.INFO if authorized else logging.WARNING,
            "Access %s for chat ID: %s",
            "granted" if authorized else "denied",
            chat_id,
        )

        if authorized:
            chat_state = self._get_or_create_chat_state(chat_id)
            system_message = self._create_system_message()
            user_message = Message(
                role=Role.USER,
                content=f"I am @{update.message.from_user.username}. {text}",
            )

            if chat_state.history_enabled:
                chat_state.messages.append(user_message)

            messages = (
                chat_state.messages
                if chat_state.history_enabled
                else [system_message, user_message]
            )

            if await self._should_process_message(update):
                result = await self._process_message_with_error_handling(
                    messages, update, context
                )
                await context.bot.send_message(chat_id=chat_id, text=result)

                if chat_state.history_enabled:
                    chat_state.messages.append(
                        Message(role=Role.ASSISTANT, content=result)
                    )
        else:
            logging.warning(
                "Chat ID not allowed or authorization failed for ID: %s", chat_id
            )

    def _get_or_create_chat_state(self, chat_id: int) -> ChatState:
        chat_state = self._state.chats.get(chat_id)
        if not chat_state:
            chat_state = ChatState(
                messages=[self._create_system_message()],
                history_enabled=self._state.default_history_enabled,
                respond_to_all=self._state.default_respond_to_all,
            )
            self._state.chats[chat_id] = chat_state
        return chat_state

    def _create_system_message(self) -> Message:
        return Message(
            role=Role.SYSTEM,
            content=f"Your username is @{self._config.bot_name}. {self._config.system_message}",
        )

    async def _process_message_with_error_handling(
        self, messages: Messages, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> str:
        try:
            return await self._process_message(messages, update, context)
        except Exception as e:
            logging.error("Error processing message: %s", str(e))
            return f"@{update.message.from_user.username} An error occurred"

    @abstractmethod
    async def _process_message(
        self, messages: Messages, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> str:
        pass

    async def _should_process_message(self, update: Update) -> bool:
        return True

    async def _is_authorized(self, update: Update) -> bool:
        return (
            self._config.allowed_chat_ids == ALL
            or update.effective_chat.id in self._config.allowed_chat_ids
        )
