from telegram import Update
from telegram.ext import ContextTypes

from llm.type import Messages
from .history import TelegramHistoryCommand


class EchoTelegramBotCommand(TelegramHistoryCommand):
    """Handles the echoing of messages in a Telegram bot."""

    async def _process_message(
        self, messages: Messages, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> str:
        response = await self._llm_service.infer(messages)
        username = update.message.from_user.username
        tagged_username = f"@{username}"
        return (
            response
            if response.startswith(tagged_username)
            else f"{tagged_username} {response}"
        )

    async def _should_process_message(self, update: Update) -> bool:
        chat = self._state.chats.get(update.effective_chat.id)
        respond_to_all = (
            chat.respond_to_all if chat else self._state.default_respond_to_all
        )

        return respond_to_all or update.message.text.startswith(
            f"@{self._config.bot_name}"
        )
