from dataclasses import dataclass, field
from typing import Dict

from llm.type import Message


@dataclass
class ChatState:
    messages: list[Message]
    history_enabled: bool
    respond_to_all: bool


@dataclass
class ChatsState:
    default_history_enabled: bool
    default_respond_to_all: bool
    chats: Dict[int, ChatState] = field(default_factory=dict)
