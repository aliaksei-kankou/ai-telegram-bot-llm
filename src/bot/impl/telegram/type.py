from dataclasses import dataclass


@dataclass
class TelegramBotConfig:
    message_poll_interval: float
