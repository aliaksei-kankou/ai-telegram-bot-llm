from telegram.ext import Application

from ...interface.decorator import BotDecorator
from .type import TelegramBotConfig


class TelegramBotDecorator(BotDecorator[Application]):
    def __init__(self, bot: Application, config: TelegramBotConfig):
        super().__init__(bot)
        self._config = config

    def run(self):
        self._bot.run_polling(poll_interval=self._config.message_poll_interval)
