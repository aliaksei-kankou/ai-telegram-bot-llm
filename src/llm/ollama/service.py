import aiohttp

from ..service import LlmService
from ..type import Messages
from .converter import transform_messages


class OllamaLlmService(LlmService):
    async def infer(self, messages: Messages) -> str:
        data = {
            "model": self._config.model_name,
            "messages": transform_messages(messages),
            "stream": False,
        }

        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {self._config.llm_token}",
        }

        async with aiohttp.ClientSession() as session:
            async with session.post(
                self._config.llm_url, headers=headers, json=data
            ) as response:
                payload = await response.json()
                return payload.get("message").get("content")
