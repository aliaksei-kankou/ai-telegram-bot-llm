from ..type import Messages
from .type import OllamaMessage


def transform_messages(messages: Messages) -> list[OllamaMessage]:
    return [
        {"role": message.role.value, "content": message.content} for message in messages
    ]
