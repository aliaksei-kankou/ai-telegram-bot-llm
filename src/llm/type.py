from dataclasses import dataclass
from typing import Optional
from enum import Enum


@dataclass
class ServiceConfig:
    model_name: str
    system_message: str
    llm_token: str
    llm_url: Optional[str] = None


class Role(Enum):
    SYSTEM = "system"
    USER = "user"
    ASSISTANT = "assistant"


class LlmEngine(Enum):
    OLLAMA = "ollama"
    OPENAI = "openai"
    ANTHROPIC = "anthropic"


@dataclass
class Message:
    role: Role
    content: str


Messages = list[Message]
