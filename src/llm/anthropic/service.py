from anthropic import AsyncAnthropic

from ..service import LlmService
from .converter import transform_messages
from ..type import Messages, Message, ServiceConfig, Role


class AnthropicLlmService(LlmService):
    def __init__(self, config: ServiceConfig):
        super().__init__(config)
        self._client = AsyncAnthropic(api_key=config.llm_token)

    async def infer(self, messages: Messages) -> str:
        self._validate_messages(messages)

        user_assistant_messages = messages[1:]
        transformed_messages = transform_messages(user_assistant_messages)

        return await self._create_message(transformed_messages, messages[0])

    def _validate_messages(self, messages: Messages):
        if not messages:
            raise ValueError("Should have at least one message")
        if messages[0].role != Role.SYSTEM:
            raise ValueError("The first message should be a system message")

    async def _create_message(
        self, user_assistant_messages: Messages, system_message: Message
    ) -> str:
        message_response = await self._client.messages.create(
            max_tokens=1024,
            messages=user_assistant_messages,
            model=self._config.model_name,
            system=system_message,
        )
        return message_response.content[0].text
