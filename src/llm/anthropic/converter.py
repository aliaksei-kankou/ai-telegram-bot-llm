from anthropic.types import MessageParam, TextBlockParam

from ..type import Messages


def transform_messages(messages: Messages) -> list[MessageParam]:
    transformed_messages = []
    start_index = 0

    while start_index < len(messages):
        current_role = messages[start_index].role
        current_content = []

        while (
            start_index < len(messages) and messages[start_index].role == current_role
        ):
            current_content.append(
                TextBlockParam(type="text", text=messages[start_index].content)
            )
            start_index += 1

        transformed_messages.append(
            MessageParam(role=current_role.value, content=current_content)
        )

    return transformed_messages
