from .service import LlmService
from .ollama.service import OllamaLlmService
from .openai.service import OpenaiLlmService
from .anthropic.service import AnthropicLlmService
from .type import ServiceConfig, LlmEngine


class LlmServiceFactory:
    """Factory class for creating instances of LlmService based on a given engine name."""

    def __init__(self, config: ServiceConfig):
        self._config = config
        self._services = {
            LlmEngine.OLLAMA.value: OllamaLlmService,
            LlmEngine.OPENAI.value: OpenaiLlmService,
            LlmEngine.ANTHROPIC.value: AnthropicLlmService,
        }

    def get(self, name: str) -> LlmService:
        if service_class := self._services.get(name):
            return service_class(self._config)
        raise Exception(f"Handler not found for engine: {name}.")
