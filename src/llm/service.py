from abc import ABC, abstractmethod

from .type import ServiceConfig, Messages


class LlmService(ABC):
    """Abstract base class for a large language model service."""

    def __init__(self, config: ServiceConfig):
        self._config = config

    @abstractmethod
    async def infer(self, messages: Messages) -> str:
        pass
