from openai import AsyncOpenAI

from ..service import LlmService
from .converter import transform_messages
from ..type import Messages, ServiceConfig


class OpenaiLlmService(LlmService):
    def __init__(self, config: ServiceConfig):
        super().__init__(config)
        self._client = AsyncOpenAI(api_key=config.llm_token)

    async def infer(self, messages: Messages) -> str:
        response = await self._client.chat.completions.create(
            model=self._config.model_name,
            messages=transform_messages(messages),
            temperature=0.7,
        )

        return response.choices[0].message.content
