from typing import Union, Iterable

from openai.types.chat import (
    ChatCompletionSystemMessageParam,
    ChatCompletionUserMessageParam,
    ChatCompletionAssistantMessageParam,
    ChatCompletionToolMessageParam,
    ChatCompletionFunctionMessageParam,
)

from ..type import Messages, Role


def transform_messages(
    prompt: Messages,
) -> Iterable[
    Union[
        ChatCompletionSystemMessageParam,
        ChatCompletionUserMessageParam,
        ChatCompletionAssistantMessageParam,
        ChatCompletionToolMessageParam,
        ChatCompletionFunctionMessageParam,
    ]
]:
    transformed_prompt = []
    for message in prompt:
        if message.role == Role.SYSTEM:
            transformed_prompt.append(
                ChatCompletionSystemMessageParam(
                    content=message.content, role=message.role.value
                )
            )
        elif message.role == Role.USER:
            transformed_prompt.append(
                ChatCompletionUserMessageParam(
                    content=message.content, role=message.role.value
                )
            )
        elif message.role == Role.ASSISTANT:
            transformed_prompt.append(
                ChatCompletionAssistantMessageParam(
                    content=message.content, role=message.role.value
                )
            )
    return transformed_prompt
