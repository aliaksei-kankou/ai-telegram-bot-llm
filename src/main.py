import logging

from container import Container

logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":
    container = Container()
    runner = container.runner
    runner.run()
