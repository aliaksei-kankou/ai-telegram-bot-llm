from telegram.ext import ApplicationBuilder, CommandHandler, MessageHandler, filters

from config import Config
from llm.service_factory import LlmServiceFactory
from llm.type import ServiceConfig
from runner import Runner
from bot.impl.telegram.decorator import TelegramBotDecorator
from bot.impl.telegram.command.echo import EchoTelegramBotCommand
from bot.impl.telegram.command.clear_history import ClearHistoryTelegramBotCommand
from bot.impl.telegram.command.enable_history import EnableHistoryTelegramBotCommand
from bot.impl.telegram.command.disable_history import DisableHistoryTelegramBotCommand
from bot.impl.telegram.command.respond_to_all import RespondToAllTelegramBotCommand
from bot.impl.telegram.command.respond_on_tag import RespondOnTagTelegramBotCommand
from bot.type import CommandConfig
from bot.impl.telegram.command.type import ChatsState
from bot.impl.telegram.type import TelegramBotConfig


class Container:
    """
    A dependency injection container class for a Telegram bot application. It manages the creation and assembly
    of various service components and configurations required by the application.
    """

    def __init__(self):
        self._config = Config()
        self._llm_service = self._create_llm_service()
        self._telegram_bot = self._create_telegram_bot()
        self._runner = Runner(self._telegram_bot)

    @property
    def runner(self):
        """Provides public access to the runner instance."""
        return self._runner

    def _create_llm_service(self):
        service_config = ServiceConfig(
            model_name=self._config.get("model_name"),
            system_message=self._config.get("system_message"),
            llm_token=self._config.get("llm_token"),
            llm_url=self._config.get("llm_url"),
        )
        llm_service_factory = LlmServiceFactory(service_config)
        return llm_service_factory.get(self._config.get("llm_engine"))

    def _create_telegram_bot(self):
        app = (
            ApplicationBuilder()
            .token(self._config.get("bot_token"))
            .concurrent_updates(self._config.get("concurrent_updates"))
            .build()
        )
        command_config = self._create_command_config()
        chats_state = ChatsState(
            default_respond_to_all=self._config.get("respond_to_all"),
            default_history_enabled=self._config.get("enable_history"),
        )

        commands = {
            "clear_history": ClearHistoryTelegramBotCommand,
            "enable_history": EnableHistoryTelegramBotCommand,
            "disable_history": DisableHistoryTelegramBotCommand,
            "respond_to_all": RespondToAllTelegramBotCommand,
            "respond_on_tag": RespondOnTagTelegramBotCommand,
        }
        for command_name, command_class in commands.items():
            command_instance = command_class(
                command_config, self._llm_service, chats_state
            )
            handler = CommandHandler(command_name, command_instance.execute)
            app.add_handler(handler)

        echo_command = EchoTelegramBotCommand(
            command_config, self._llm_service, chats_state
        )
        echo_handler = MessageHandler(
            filters.TEXT & (~filters.COMMAND), echo_command.execute
        )
        app.add_handler(echo_handler)

        return TelegramBotDecorator(
            app,
            TelegramBotConfig(
                message_poll_interval=self._config.get("message_poll_interval")
            ),
        )

    def _create_command_config(self):
        return CommandConfig(
            allowed_chat_ids=self._config.get("allowed_chat_ids"),
            bot_name=self._config.get("bot_name"),
            system_message=self._config.get("system_message"),
            clear_history_message=self._config.get("clear_history_message"),
            enable_history_message=self._config.get("enable_history_message"),
            disable_history_message=self._config.get("disable_history_message"),
            respond_to_all_message=self._config.get("respond_to_all_message"),
            respond_on_tag_message=self._config.get("respond_on_tag_message"),
        )
