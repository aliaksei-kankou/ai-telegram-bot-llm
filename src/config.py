import os
from typing import Optional, Union, List


class Config:
    """
    Configuration manager class that loads environment variables and provides access
    to various settings needed for bot and LLM operations.
    """

    def __init__(self):
        self._load_bot_config()
        self._load_llm_config()
        self._load_message_config()

    def _load_bot_config(self):
        """Loads bot-related configuration."""
        self.bot_name: str = os.getenv("BOT_NAME")
        self.bot_token: str = os.getenv("BOT_TOKEN")
        allowed_chat_ids: str = os.getenv("ALLOWED_CHAT_IDS")

        if allowed_chat_ids == "*":
            self.allowed_chat_ids: Union[str, List[int]] = "*"
        else:
            self.allowed_chat_ids = self._parse_chat_ids(allowed_chat_ids)

    def _load_llm_config(self):
        """Loads LLM-related configuration."""
        self.llm_token: str = os.getenv("LLM_TOKEN")
        self.llm_url: str = os.getenv("LLM_URL")
        self.llm_engine: str = os.getenv("LLM_ENGINE")
        self.model_name: str = os.getenv("MODEL_NAME")
        self.system_message: str = os.getenv("SYSTEM_MESSAGE", "")

    def _load_message_config(self):
        """Loads message-related settings."""
        self.clear_history_message: str = os.getenv("CLEAR_HISTORY_MESSAGE", "✅")
        self.enable_history_message: str = os.getenv("ENABLE_HISTORY_MESSAGE", "✅")
        self.disable_history_message: str = os.getenv("DISABLE_HISTORY_MESSAGE", "✅")
        self.respond_to_all_message: str = os.getenv("RESPOND_TO_ALL_MESSAGE", "✅")
        self.respond_on_tag_message: str = os.getenv("RESPOND_ON_TAG_MESSAGE", "✅")

        self.enable_history: bool = (
            os.getenv("ENABLE_HISTORY", "false").lower() == "true"
        )
        self.concurrent_updates: bool = (
            os.getenv("CONCURRENT_UPDATES", "false").lower() == "true"
        )
        self.respond_to_all: bool = (
            os.getenv("RESPOND_TO_ALL", "false").lower() == "true"
        )
        self.message_poll_interval: float = float(
            os.getenv("MESSAGE_POLL_INTERVAL", "1.0")
        )

    def _parse_chat_ids(self, chat_ids: str) -> List[int]:
        return [int(id.strip()) for id in chat_ids.split(",") if id.strip()]

    def get(self, name: str) -> Optional[Union[bool, float, str, List[int]]]:
        return getattr(self, name, None)
