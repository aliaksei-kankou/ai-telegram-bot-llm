from bot.interface.decorator import BotDecorator


class Runner:
    def __init__(self, bot: BotDecorator):
        self._bot = bot

    def run(self):
        self._bot.run()
