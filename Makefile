.PHONY: setup test clean all format check_format coverage_report code_quality security_checks check_lint

# Set the name of the python environment directory
VENV := .venv

# Set the name of the python executable
PYTHON := python3

# Set PYTHONPATH to the src directory
export PYTHONPATH := ./src

# Minimum pylint score required
MIN_PYLINT_SCORE := 9.2

# Minimum coverage percentage required
COVERAGE_THRESHOLD := 97

all: clean setup format lint code_quality security_checks test coverage_report

# Setup virtual environment and install dependencies
setup:
	@test -d $(VENV) || $(PYTHON) -m venv $(VENV)
	@. $(VENV)/bin/activate && pip install -r make.requirements.txt -r src/requirements.txt

test: setup
	@echo "Running tests..."
	@. $(VENV)/bin/activate && coverage run -m pytest

coverage_report: setup
	@echo "Generating coverage report..."
	@. $(VENV)/bin/activate && coverage run -m pytest && coverage report && coverage html
	@echo "Coverage report generated in htmlcov/index.html"

format: setup
	@echo "Formatting code with black..."
	@. $(VENV)/bin/activate && black src test

check_format: setup
	@. $(VENV)/bin/activate && black --check src test

check_lint: setup
	@echo "Checking pylint score..."
	@score=$$($(VENV)/bin/pylint src | grep "Your code has been rated at" | awk '{print $$7}' | sed 's,/.*,,' ); \
	if [ $(echo "$$score < $(MIN_PYLINT_SCORE)" | bc -l) ]; then \
		echo "Pylint score ($$score) is below the minimum required ($(MIN_PYLINT_SCORE))"; \
		exit 1; \
	else \
		echo "Pylint score ($$score) meets the minimum required ($(MIN_PYLINT_SCORE))"; \
	fi

code_quality: setup
	@echo "Running code quality checks..."
	@. $(VENV)/bin/activate && flake8 src test --max-line-length=120 --exclude=__init__.py --extend-ignore=F401

security_checks: setup
	@echo "Running security checks..."
	@. $(VENV)/bin/activate && bandit -r src

check_coverage: setup
	@echo "Checking test coverage..."
	@. $(VENV)/bin/activate && coverage report --fail-under=$(COVERAGE_THRESHOLD)

clean:
	@echo "Removing virtual environment and other generated files..."
	@rm -rf $(VENV) assets .venv dist report.html htmlcov .coverage
