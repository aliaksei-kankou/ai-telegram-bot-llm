import pytest
from llm.anthropic.converter import transform_messages
from anthropic.types import MessageParam, TextBlockParam
from llm.type import Messages, Message, Role


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "messages, expected",
    [
        (
            Messages(
                [
                    Message(role=Role.ASSISTANT, content="Assistant message 1"),
                    Message(role=Role.ASSISTANT, content="Assistant message 2"),
                    Message(role=Role.USER, content="User message 1"),
                    Message(role=Role.USER, content="User message 2"),
                    Message(role=Role.USER, content="User message 3"),
                ]
            ),
            [
                MessageParam(
                    role=Role.ASSISTANT.value,
                    content=[
                        TextBlockParam(type="text", text="Assistant message 1"),
                        TextBlockParam(type="text", text="Assistant message 2"),
                    ],
                ),
                MessageParam(
                    role=Role.USER.value,
                    content=[
                        TextBlockParam(type="text", text="User message 1"),
                        TextBlockParam(type="text", text="User message 2"),
                        TextBlockParam(type="text", text="User message 3"),
                    ],
                ),
            ],
        ),
        (
            Messages(
                [
                    Message(role=Role.USER, content="I am user one."),
                    Message(role=Role.USER, content="I am user two."),
                    Message(role=Role.USER, content="I am user three."),
                ]
            ),
            [
                MessageParam(
                    role=Role.USER.value,
                    content=[
                        TextBlockParam(type="text", text="I am user one."),
                        TextBlockParam(type="text", text="I am user two."),
                        TextBlockParam(type="text", text="I am user three."),
                    ],
                )
            ],
        ),
    ],
    ids=["HappyPath-1", "HappyPath-2"],
)
def test_transform_messages_happy_path(messages, expected):
    # Act
    result = transform_messages(messages)

    # Assert
    assert (
        result == expected
    ), "The transformed messages do not match the expected output."
