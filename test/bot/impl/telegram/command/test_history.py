import pytest
from unittest.mock import AsyncMock, MagicMock

from telegram import Update, User, Chat
from telegram.ext import ContextTypes

from llm.service import LlmService
from llm.type import Messages
from bot.impl.telegram.command.history import TelegramHistoryCommand
from bot.impl.telegram.command.type import ChatsState
from bot.type import CommandConfig


PROCESSED_MESSAGE = "Processed message"


class ConcreteTelegramHistoryCommand(TelegramHistoryCommand):
    async def _process_message(
        self, messages: Messages, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> str:
        return PROCESSED_MESSAGE


# Mock setup for dependencies
@pytest.fixture
def command_config():
    return CommandConfig(
        bot_name="TestBot",
        system_message="Welcome to TestBot.",
        allowed_chat_ids=[123, 456],
        clear_history_message="Clearing history...",
        enable_history_message="History enabled.",
        disable_history_message="History disabled.",
        respond_to_all_message="Responding to all.",
        respond_on_tag_message="Responding on tag.",
    )


@pytest.fixture
def llm_service():
    return MagicMock(spec=LlmService)


@pytest.fixture
def chats_state():
    return ChatsState(
        chats={}, default_history_enabled=True, default_respond_to_all=False
    )


@pytest.fixture
def update():
    user = User(id=123, is_bot=False, first_name="John", username="john_doe")
    chat = Chat(id=456, type="private")
    return Update(
        update_id=1, message=MagicMock(chat=chat, text="Hello, bot!", from_user=user)
    )


@pytest.fixture
def context():
    context = MagicMock(spec=ContextTypes.DEFAULT_TYPE)
    context.bot = MagicMock()
    context.bot.send_message = AsyncMock()
    return context


# Parametrized tests
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "authorized,history_enabled,should_process_message,expected_call_count,expected_messages",
    [
        (
            True,
            True,
            True,
            1,
            [
                "Your username is @TestBot. Welcome to TestBot.",
                "I am @john_doe. Hello, bot!",
            ],
        ),
        (
            True,
            False,
            True,
            1,
            [
                "Your username is @TestBot. Welcome to TestBot.",
                "I am @john_doe. Hello, bot!",
            ],
        ),
        (False, True, True, 0, []),
        (
            True,
            True,
            False,
            0,
            [
                "Your username is @TestBot. Welcome to TestBot.",
                "I am @john_doe. Hello, bot!",
            ],
        ),
    ],
)
async def test_telegram_history_command(
    authorized,
    history_enabled,
    should_process_message,
    expected_call_count,
    expected_messages,
    command_config,
    llm_service,
    chats_state,
    update,
    context,
):
    # Arrange
    command = ConcreteTelegramHistoryCommand(command_config, llm_service, chats_state)
    command._is_authorized = AsyncMock(return_value=authorized)
    command._should_process_message = AsyncMock(return_value=should_process_message)
    command._process_message_with_error_handling = AsyncMock(
        return_value=PROCESSED_MESSAGE
    )

    # Act
    await command.execute(update, context)

    # Assert
    assert context.bot.send_message.call_count == expected_call_count
    if authorized and history_enabled and should_process_message:
        print(chats_state.chats[456].messages)
        assert len(chats_state.chats[456].messages) == len(expected_messages) + 1
        for message in chats_state.chats[456].messages:
            assert message.content in [*expected_messages, PROCESSED_MESSAGE]
