import pytest
from unittest.mock import MagicMock, AsyncMock, call
from telegram import Update, Message, User, Chat
from telegram.ext import ContextTypes
from bot.impl.telegram.command.type import ChatsState
from bot.type import CommandConfig
from llm.type import Messages
from bot.impl.telegram.command.history import TelegramHistoryCommand


@pytest.fixture
def mock_update():
    user = User(id=1, first_name="username", username="username", is_bot=False)
    chat = Chat(id=1, type="private")
    message = Message(message_id=1, date=None, chat=chat, from_user=user, text="text")
    return Update(update_id=1, message=message)


@pytest.mark.asyncio
async def test_telegram_history_process_message_raise_exception(mock_update):
    class ConcreteTelegramHistoryCommand(TelegramHistoryCommand):
        async def _process_message(
            self, messages: Messages, update: Update, context: ContextTypes.DEFAULT_TYPE
        ) -> str:
            raise Exception("Exception")

        async def _is_authorized(self, update: Update) -> bool:
            return True

    command_config = CommandConfig(
        allowed_chat_ids=[1],
        bot_name="bot_name",
        system_message="",
    )

    state = ChatsState(
        default_respond_to_all=True,
        default_history_enabled=True,
    )

    command = ConcreteTelegramHistoryCommand(command_config, None, state)

    context = MagicMock()
    context.bot.send_message = AsyncMock(return_value=None)

    await command.execute(mock_update, context)

    context.bot.send_message.assert_has_calls(
        [call(chat_id=1, text="@username An error occurred")]
    )


@pytest.mark.asyncio
async def test_telegram_history_process_message_happy(mock_update):
    class ConcreteTelegramHistoryCommand(TelegramHistoryCommand):
        async def _process_message(
            self, messages: Messages, update: Update, context: ContextTypes.DEFAULT_TYPE
        ) -> str:
            return "Processed message"

        async def _is_authorized(self, update: Update) -> bool:
            return True

    command_config = CommandConfig(
        allowed_chat_ids=[1],
        bot_name="bot_name",
        system_message="",
    )

    state = ChatsState(
        default_respond_to_all=True,
        default_history_enabled=True,
    )

    command = ConcreteTelegramHistoryCommand(command_config, None, state)

    context = MagicMock()
    context.bot.send_message = AsyncMock(return_value=None)

    await command.execute(mock_update, context)

    context.bot.send_message.assert_has_calls(
        [call(chat_id=1, text="Processed message")]
    )
